import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TemplateListComponent } from './template-list/template-list.component';

import {TemplateService} from "../assets/template.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    TemplateListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [TemplateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
