import { Component, OnInit } from '@angular/core';
import {TemplateService} from "../../assets/template.service";
import {Template} from "../../assets/template";

@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.css']
})
export class TemplateListComponent implements OnInit {
    template: Template[];
    errorMessage: string;

  constructor(private service: TemplateService) {}

  ngOnInit() {
      this.getData("http://localhost:2403/my-objects");
  }

    private getData(url) {
        this.service.getTemplates(url).subscribe(
            (items) => {
                this.template = items;
            },
            // (items)=>{
            //     if(items.length == undefined){
            //         alert("Can`t get data");
            //     }
            // }
        );
    }


}
