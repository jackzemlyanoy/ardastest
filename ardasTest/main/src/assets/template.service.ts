import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Template } from "./template";

import { catchError, map } from 'rxjs/internal/operators'

@Injectable()
export class TemplateService {

    // адрес сервиса
    private url = "http://localhost:2403/my-objects";

    constructor(private http: HttpClient) { }

    // Отправка GET запроса нв сервер
    public getTemplates(varUrl) {
        return this.http.get(this.url)
            .pipe(
                // catchError(this.handleError),
                map((result: any) => result)
            )
    }

    // // Отправка GET запроса нв сервер
    // public getTemplates(): Observable<Template[]> {
    //     let templates = this.http.get(this.url)
    //         .map(this.extractTemplates) // преобразовывает ответ в массив экземпляров Template.
    //         .catch(this.handleError);
    //     return templates;
    // }
    //
    // public getTemplate(id: string): Observable<Template> {
    //     let template = this.http.get(this.url + "/" + id)
    //         .map(this.extractTemplate) // преобразовывает ответ в экземпляр Template.
    //         .catch(this.handleError);
    //     return template;
    // }
    //
    //
    // // Отправка PUT запроса и обновление продукта в базе.
    // public updateTemplate(template: Template): Observable<Template> {
    //     return this.http.put(this.url + "/" + template.id, template)
    //         .catch(this.handleError);
    // }
    //
    //
    // private extractTemplates(response: Response) {
    //     let res = response.json();
    //     let templates: Template[] = [];
    //     for (let i = 0; i < res.length; i++) {
    //         templates.push(new Template(res[i].id, res[i].name, res[i].template, res[i].modified));
    //     }
    //     return templates;
    // }
    //
    // private extractTemplate(response: Response) {
    //     let res = response.json();
    //     let template = new Template(res.id, res.name, res.template, res.modified);
    //     return template;
    // }
    //
    // private handleError(error: any, cought: Observable<any>): any {
    //     let message = "";
    //
    //     if (error instanceof Response) {
    //         let errorData = error.json().error || JSON.stringify(error.json());
    //         message = `${error.status} - ${error.statusText || ''} ${errorData}`
    //     } else {
    //         message = error.message ? error.message : error.toString();
    //     }
    //
    //     console.error(message);
    //
    //     return Observable.throw(message);
    // }
}
