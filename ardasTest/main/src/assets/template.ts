export class Template {
    public id: string;
    public name: string;
    public template: string;
    public modified: number;

    constructor(id, name, template, modified) {
        this.id = id;
        this.name = name;
        this.template = template;
        this.modified = modified;
    }
}